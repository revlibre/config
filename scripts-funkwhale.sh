#!/bin/bash

#upgrade music@revlibre
curl -o /tmp/upgrade.sh -sSL "https://get.funkwhale.audio/upgrade.sh"  && sudo sh /tmp/upgrade.sh develop

# see journal entries
sudo journalctl -xn -u funkwhale-worker -f
sudo journalctl -xn -u funkwhale-server -f

#certbot-renew (in cron.weekly)
certbot-auto -v -n --renew certonly --nginx -m eda@mutu.net --agree-tos -d music.revlibre.org

